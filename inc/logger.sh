 #!/bin/bash
#---------------------------------------------------------------
# setup.sh
# Logger class for bash
# Date:         24 October 2011
# Createur: Xavier Garcia <x@vier.im>
#---------------------------------------------------------------

function log() {
    if [ $# -eq 2 ]; then
        case "${1}" in
            ERROR )
                if [ "${logLevel}" = "STDOUT" ]; then 
                   echo "$1 [`date +'%d-%b-%y %T'`] $2" | tee -a ${LogFile}
                else
                   echo "$1 [`date +'%d-%b-%y %T'`] $2" | tee -a ${LogFile} >> /dev/null
                fi
                ;;
            WARN )
                if [ "${logLevel}" = "STDOUT" ]; then 
                   echo "$1 [`date +'%d-%b-%y %T'`] $2" | tee -a ${LogFile}
                elif [ "${logLevel}" = "WARN" -o "${logLevel}" = "INFO" -o "${logLevel}" = "DEBUG" ]; then 
                    echo "$1 [`date +'%d-%b-%y %T'`] $2" | tee -a ${LogFile} >> /dev/null
                fi
                ;;
            INFO )
                if [ "${logLevel}" = "STDOUT" ]; then 
                   echo "$1 [`date +'%d-%b-%y %T'`] $2" | tee -a ${LogFile} 
                elif [ "${logLevel}" = "INFO" -o "${logLevel}" = "DEBUG" ]; then 
                    echo "$1 [`date +'%d-%b-%y %T'`] $2" | tee -a ${LogFile} >> /dev/null
                fi
                ;;
            DEBUG )
                if [ "${logLevel}" = "STDOUT" ]; then 
                   echo "$1 [`date +'%d-%b-%y %T'`] $2" | tee -a ${LogFile}
                elif [ "${logLevel}" = "DEBUG" ]; then 
                    echo "$1 [`date +'%d-%b-%y %T'`] $2" | tee -a ${LogFile} >> /dev/null
                fi
                ;;
            *)
                if [ "${logLevel}" = "STDOUT" ]; then 
                   echo "$DEBUG [`date +'%d-%b-%y %T'`] $1 - $2" | tee -a ${LogFile}
                elif [ "${logLevel}" = "DEBUG" ]; then 
                    echo "DEBUG [`date +'%d-%b-%y %T'`] $1 - $2" | tee -a ${LogFile} >> /dev/null
                fi
                ;;
        esac
    else
        if [ "${logLevel}" = "STDOUT" ]; then 
           echo "$1 [`date +'%d-%b-%y %T'`] $@" | tee -a ${LogFile}
        elif [ "${logLevel}" = "DEBUG" ]; then 
            echo "DEBUG [`date +'%d-%b-%y %T'`] $@" | tee -a ${LogFile} >> /dev/null
        fi
    fi
}

function DEBUG() {
    msg=$@
    IFS=$'/'
    srcFileFullPath=${BASH_SOURCE[1]}
    set -- $srcFileFullPath
    arr=( $srcFileFullPath )
    unset IFS
    arrLength=${#arr[@]}
    srcFile=${arr[$arrLength-1]}
    log "DEBUG" "${srcFile}:${FUNCNAME[1]}:${BASH_LINENO[0]} - $msg"
}
function INFO() {
    msg=$@
    IFS=$'/'
    srcFileFullPath=${BASH_SOURCE[1]}
    set -- $srcFileFullPath
    arr=( $srcFileFullPath )
    unset IFS
    arrLength=${#arr[@]}
    srcFile=${arr[$arrLength-1]}
    log "INFO" " ${srcFile}:${FUNCNAME[1]}:${BASH_LINENO[0]} - $msg"
}

function WARN() {
    msg=$@
    IFS=$'/'
    srcFileFullPath=${BASH_SOURCE[1]}
    set -- $srcFileFullPath
    arr=( $srcFileFullPath )
    unset IFS
    arrLength=${#arr[@]}
    srcFile=${arr[$arrLength-1]}
    log "WARN" " ${srcFile}:${FUNCNAME[1]}:${BASH_LINENO[0]} - $msg"
}

function ERROR() {
    msg=$@
    IFS=$'/'
    srcFileFullPath=${BASH_SOURCE[1]}
    set -- $srcFileFullPath
    arr=( $srcFileFullPath )
    unset IFS

    arrLength=${#arr[@]}
    srcFile=${arr[$arrLength-1]}
    log "ERROR" "${srcFile}:${FUNCNAME[1]}:${BASH_LINENO[0]} - $msg"
}


