#!/bin/bash
#---------------------------------------------------------------
#  APT
#---------------------------------------------------------------

LISTE=""
# Essential
LISTE=$LISTE" build-essential libpcre3-dev pkg-config libglib2.0-dev libxml2-dev libxml2 libmemcache-dev libmemcache0 libssl-dev libsqlite3-dev"
LISTE=$LISTE" libpcre3-dev pkg-config libglib2.0-dev libxml2-dev libxml2 libmemcache-dev libmemcache0 libssl-dev libsqlite3-dev"
LISTE=$LISTE" cron-apt make dpkg-dev gcc g++ libc6-dev libncurses5-dev lzma vim"
# Network
LISTE=$LISTE" snmpd curl nmap traceroute fail2ban postfix" 
# Tools
LISTE=$LISTE" mc dstat rrdtool byobu htop mytop mailutils unzip zip"
# Service 
LISTE=$LISTE" subversion openvpn ntpdate sudo rsync"
#echo ${LISTE}
INFO "init list pack"

# Color SVN

function color-svn() {
  if [ ! -e /usr/bin/colorsvn ]; then
    INFO "Read color svn function"
    cd /tmp
    INFO "Download svn color"
    wget "http://www.console-colors.de/downloads/colorsvn/colorsvn-0.3.2.tar.gz" > /dev/null 2>&1
    tar xvzf colorsvn-0.3.2.tar.gz  > /dev/null 2>&1
    cd colorsvn-0.3.2
    INFO "Install svn color"
    ./configure   > /dev/null 2>&1
    make   > /dev/null 2>&1
    make install  > /dev/null 2>&1
    rm -rf /tmp/colorsvn-0.3.2.tar.gz
    rm -rf /tmp/colorsvn-0.3.2
  else
    INFO "SVN COLORE Already existe"
  fi
}

function apt () {
if [ ! -e "/tmp/aptdone" ]; then
  touch /tmp/aptdone
  WARN "LISTE Software: ${LISTE}"
  INFO "aptdone setted"	
  apt-get -y update  > /dev/null 2>&1
  INFO "Update repository"
  cecho "Update repository"    ${CYAN}
  apt-get -y --force-yes upgrade  > /dev/null 2>&1
  INFO "Distribution updrage"
  cecho "Upgrade Distribution"    ${CYAN}
  apt-get -y --force-yes install ${LISTE}  > /dev/null 2>&1
  apt-get -y install ${LISTEVERB}   
  cecho "Install List software"   ${CYAN} 
  INFO "Install List package"
fi
}
# Install 
apt
color-svn


