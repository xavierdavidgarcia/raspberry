###################################
# Variable
###################################
USERNAME="ivid"
HOSTNAME="lb-img2-fr"


#-----------------------------------
# Logger Var 
#-----------------------------------
# Source the logger
# Usage:
    #DEBUG "debug message"
    #INFO "info message"
    #WARN "warn message"
    #ERROR "error message"
logLevel=DEBUG    
GeneralFolderLog=/tmp
LogFile=${GeneralFolderLog}/script_install.log
maxLogFileSize=1000

#--------------------------
# Color
#--------------------------
RED="\033[00;31;1m"
CYAN="\033[00;36;1m"
WHITE="\033[00;37;1m"

####################################
# Function
###################################
cecho (){
local default_msg="No message passed."
message=${1:-$default_msg}   # Defaults to default message.
color=${2}           # Defaults to black, if not specified.
#  C=80;printf "$color$message\033[0m%${C}s"; printf "\033[00;32;1m[OK]\033[0m\n"
C=45;printf "$color%-45s\033[0m%${C}s\033[00;32;1m[OK]\033[0m\n" "$message";
return
}

echo "---------------------------------------------------------------------------------"
echo " 			Variable setted on VAR.sh file"
echo "---------------------------------------------------------------------------------"
echo "  USERNAME : ${USERNAME}"
echo "  HOSTNAME : ${HOSTNAME}"
echo "  LOGLEVEL: ${logLevel}"
echo "  LOG FILE: ${LogFile}"
echo "---------------------------------------------------------------------------------"
sleep 2
