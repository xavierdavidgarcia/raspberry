#!/bin/bash
#---------------------------------------------------------------
# 0-init.sh
# Init script
# Date:         30 July 2010
# Createur: Xavier Garcia <x@vier.im>
#---------------------------------------------------------------

# Log file folder
if [ ! -d ${GeneralFolderLog} ];then
  mkdir -p ${GeneralFolderLog}
fi

#---------------------------------------------------------------
# Create User
#---------------------------------------------------------------
if [ -z "${USERNAME}" ]; then
  cecho "Username not setted"   ${RED}
  WARN "Username not setted"
  sleep 5
  clear
  read USERNAME
  exit 0
fi

grep  -i "^${USERNAME}" /etc/passwd >> /dev/null
if [ $? -eq 0 ]; then
  cecho "User exists"     ${RED}
  INFO "Username already exists"
else
  echo "User not exists"
  useradd --create-home --shell /bin/bash --home /home/${USERNAME} ${USERNAME}
  chown -R ${USERNAME}. /home/${USERNAME}/
  cecho "User created" ${CYAN}
  INFO "Username created with home folder"
fi

#---------------------------------------------------------------
# Custom User
#---------------------------------------------------------------
# SSH Key
# Add ssh key
if [ -d /home/${USERNAME}/.ssh ]; then 
	rm -rf /home/${USERNAME}/.ssh  > /dev/null 2>&1
fi
if [ -d /root/.ssh ]; then
	rm -rf /root/.ssh >> /dev/null
fi
  mkdir -p /home/${USERNAME}/.ssh/  > /dev/null 2>&1
  mkdir -p /root/.ssh/			 > /dev/null 2>&1
  cecho "Create .ssh folder" ${RED}
  INFO ".ssh froce creation"

  cp ${fdinstall}/common/home/authorized_keys /home/${USERNAME}/.ssh
  cp ${fdinstall}/common/home/authorized_keys /root/.ssh
  cp ${fdinstall}/common/home/authorized_keys2 /home/${USERNAME}/.ssh
  cp ${fdinstall}/common/home/authorized_keys2 /root/.ssh

# Verifier si hostname est vide
if [ -z "${HOSTNAME}" ]; then
  cecho "Hostname not setted"   ${RED}
  sleep 5
  clear
  read HOSTNAME
  INFO "The hostname server are: ${HOSTNAME}"
fi
  INFO "The hostname server are: ${HOSTNAME}"
#grep  -i "${HOSTNAME}" /etc/hosts  > /dev/null 2>&1
#if [ $? -eq 0 ]; then
  echo ${HOSTNAME} > /etc/hostname
  INFO "Change Hostname"
  cecho "Hostname setted ${HOSTNAME}" ${CYAN}
  #sed -i "1i\127.0.0.1        "${HOSTNAME}"\n " /etc/hosts # Add lo host name
  #sed -i "2i\127.0.0.1       localhost.localdomain localhost\n " /etc/hosts # Add lo host name
  echo "127.0.0.1        ${HOSTNAME} localhost" > /etc/hosts 
  cecho "Add Hosts file" ${CYAN}
  INFO "Add hostname on /etc/hosts"
#else
#  cecho "No hostname setted"   $RED
#  ERROR "No hostname setted"
#fi
#---------------------------------------------------------------
# User Custom configuration
#---------------------------------------------------------------
#User
cp -f ${fdinstall}/Common/home/bashrc /home/${USERNAME}/.bashrc
cp -f ${fdinstall}/Common/home/nanorc /home/${USERNAME}/.nanorc
cp -f ${fdinstall}/Common/home/my.cnf /home/${USERNAME}/.my.cnf
cp -f ${fdinstall}/Common/home/screenrc /home/${USERNAME}/.screenrc
INFO "Copy user custom file"
chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}/
INFO "Chown user custom file"
# Root
cp -f ${fdinstall}/Common/home/bashrc /root/.bashrc
cp -f ${fdinstall}/Common/home/nanorc /root/.nanorc
cp -f ${fdinstall}/Common/home/my.cnf /root/.my.cnf
cp -f ${fdinstall}/Common/home/screenrc /root/.screenrc
INFO "Copy root custom file"
chown -R root:root /root
cecho "User Custom" ${CYAN}
INFO "Chown root custom file"


#---------------------------------------------------------------
# #  Custom Admin 
#---------------------------------------------------------------
# Cron admin 
if [ ! -e /etc/cron.d/admin ];then
  #touch /etc/cron.d/ivi_admin
  touch /etc/cron.d/admin
  INFO "Copie cron file configuration"
else
    INFO "Cron file Exist"
fi

# Update Source.list
#rm -rf /etc/apt/sources.list
##INFO "Remove old source.list"
#cp -rf ${fdinstall}/Common/etc/sources.list /etc/apt/sources.list
#INFO "Copy custom apt source.list"


