#!/bin/bash
#---------------------------------------------------------------
# setup.sh
# Installer for Debian raspberry
# Date:         24 October 2011
# Createur: Xavier Garcia <x@vier.im>
# Notice: Execute this:
# ./setup.sh
#---------------------------------------------------------------

###################################
# Variable
###################################
# Test que le script est lance en root
if [ $EUID -ne 0 ]; then
  echo "Script need root: # sudo $0" 1>&2
  ERROR "Script need execute in root"
  exit 1
fi
fdinstall=$(pwd)
source inc/var.sh
source inc/logger.sh
INFO "Init 0-int.sh script"
source inc/0-init.sh
INFO "END 0-int.sh script"
INFO "Init 1-soft.sh script"
source inc/1-soft.sh
INFO "END 1-soft.sh script"
#INFO "Init 2-.sh script"
#source inc/2-        
#INFO "END 2-.sh script"
#INFO "Init 3-Security.sh script"
#source inc/3-Security.sh  
#INFO "END 3-Security.sh script"
#INFO "Init 4-monitoring.sh script"  
#source inc/4-monitoring.sh
#INFO "END 4-monitoring.sh script"  
INFO "Script Ended"
cecho  "Script Ended" $RED
